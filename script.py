import string
import random
length = random.randrange(8,15); # length can vary from 8 to 14

# Lowercase; Uppercase;q Numbers; Special characters such as !@#$%^&*(){}[]


def generate_upper_and_lower(length):
  i = 0
  
  mylist = list(string.ascii_letters)
  test_list = list()
  while i<length:
    test_list.append(random.choice(mylist))
    i=i+1;
  return ''.join(test_list)
  
def generate_nos_special(length):
  i = 0
  special = ['1','2','3','4','5','6','7','8','9','10','!','@','#','$','%','^']
  
  test_list1 = list()
  while i<length:
    test_list1.append(random.choice(special))
    i=i+1;
  return ''.join(test_list1)

print(generate_upper_and_lower(length/2) + generate_nos_special(length/2))
